<?php

namespace App;

/**
 * Set functionalities supported by the theme
 */
function add_theme_supports()
{
  add_theme_support('title-tag');
  add_theme_support('post-thumbnails');
  add_theme_support('menus');
  register_nav_menu('header-top', 'Top header menu');
}

/**
 * Add CSS and JS files.
 */
function register_assets()
{
  // register CSS and JS files
  wp_register_style('bootstrap', 'https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css');
  // registe  r JS in footer (last param)
  wp_register_script('bootstrap', 'https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js', ['popper'], false, true);
  wp_register_script('popper', 'https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js', [], false, true);
  // add registered files
  wp_enqueue_style('bootstrap');
  wp_enqueue_script('bootstrap');
}

/**
 * CSS : set header top menu item class
 */
function header_top_menu_item_css_class(array $cssClasses): array {
  $cssClasses[] = 'nav-item';
  return $cssClasses;
}

/**
 * CSS : set header top menu link class
 */
function header_top_menu_link_css_class(array $attrs): array {
  $attrs['class'] = 'nav-link';
  return $attrs;
}

add_action('after_setup_theme', 'App\add_theme_supports');
add_action('wp_enqueue_scripts', 'App\register_assets');
add_filter('nav_menu_css_class', 'App\menu_item_css_class');
add_filter('nav_menu_link_attributes', 'App\menu_item_css_class');
