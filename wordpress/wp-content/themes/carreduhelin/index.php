<?php get_header() ?>
<h1>Fichier index.php</h1>
<?php if (have_posts()) : ?>

  <div class="row">

    <?php while (have_posts()) : the_post(); ?>
    <div class="col-sm-4">
      <div class="card">
        <?php the_post_thumbnail('medium', ['class' => 'card-img-top', 'alt' => '']); ?>
        <div class="card-body">
          <h5 class="card-title"><?php the_title(); ?></h5>
          <p class="card-text"><small class="text-muted"><?php the_content(); ?></small></p>
          <a href="<?php the_permalink(); ?>" class="card-link">Voir plus</a>
        </div>
      </div>
    </div>
    <?php endwhile; ?>
    
  </div>

<?php else : ?>
  <h1>Pas d'articles</h1>
<?php endif ?>
<?php get_footer() ?>